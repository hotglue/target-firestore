"""Firestore target sink class, which handles writing streams."""


import collections
from singer_sdk.sinks import BatchSink
#Firestore imports
import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore

import urllib.parse


class FirestoreSink(BatchSink):
    """Firestore target sink class."""

    max_size = 10000  # Max records to write in one batch


    def process_batch(self, context: dict) -> None:
        """Write out any prepped records and return once fully written."""
        # Sample:
        # ------
        # client.upload(context["file_path"])  # Upload file
        # Path(context["file_path"]).unlink()  # Delete local copy
        # set the collection based on current stream
        #collection = urllib.parse.quote(self.stream_name)
        #for now fixed streat name
        collection = self.stream_name

        path_prefix = self._config.get('path_prefix') or None
            
        db = firestore.client()
        #Leave this here for possible multiple collection insert/update in future. 
        #collections = db.collections()
        records = context['records']

        for record in records:
            #db.collection(f'{collection}').document(u'id').set(dict(record))
            collection_key = '/'.join(filter(None, [path_prefix, collection]))
            db.collection(collection_key).add(dict(record))

        self.logger.info(f"Uploaded {len(records)} records into {collection}")

        # Clean up records
        context["records"] = []


